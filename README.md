# pdf-singing
This app helps you to add signature to your .pdf file
***
  AFTER downloading the code go to your bash prompt and select the directory which you have downloaded the code and type the following command
***
     http-server
***
     *To start the server
***
     *copy and paste the localhost inbulid sever "http://127.0.0.1:8080"
***
 How to use pdf-singing:
 ***
    *drag and drop to upload .pdf file
***    
    *select insert annotation and add your signature
***   
   *open the left panal and click the export button to download your file
***
![Video-Gitlab-Pdf-Signing-1](/uploads/ccb3243b072199a703ed4989c671f8d2/Video-Gitlab-Pdf-Signing-1.mp4)   

***
   *demo of this project
***
![Pdf_Signing_Demo_Video-compressed](/uploads/bf812d8f51af3a4524d931ade44b544f/Pdf_Signing_Demo_Video-compressed.mp4)


